====== Blackwell 3: Blackwell Convergence ======

===== Description =====

**Blackwell Convergence**: A new film opens to rave reviews, despite its bloody history. A beautiful uptown office remains unoccupied, despite its prime location.\\
A downtown artist berates himself for selling out, while a Wall Street investor congratulates himself on a job well done.\\
Just normal life in the big city? Or is something more sinister binding these events together?\\
Bizarre connections are a dime a dozen for the Blackwell family, but just how far back to they go? Medium Rosangela Blackwell and her spirit guide Joey Mallone are about to find out.

  * age: 12y+
  * category:  adventure, esoterism, Point’n Click
  * official url: [[http://www.wadjeteyegames.com/games/blackwell-convergence/|]]
  * release date: 2009

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-blackwell-3-blackwell-convergence.sh|play-blackwell-3-blackwell-convergence.sh]] (updated on 14/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/blackwell_bundle|gog_blackwell_convergence_2.0.0.2.sh]] (MD5: 784f9a8cf70213c938c801dadcd0b5e3)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/blackwell-3-blackwell-convergence/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/blackwell-3-blackwell-convergence/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_blackwell_convergence_2.0.0.2.sh
libplayit2.sh
play-blackwell-3-blackwell-convergence.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-blackwell-3-blackwell-convergence.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
