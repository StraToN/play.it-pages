====== Multiwinia ======
//version sold on GOG//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-multiwinia.sh|play-multiwinia.sh]] (updated on 27/08/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * gog_multiwinia_2.0.0.5.sh (MD5: ec7f0cc245b4fb4bf85cba5fc4a536ba)
  * dependencies:
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/multiwinia/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/multiwinia/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and installer:<code>
$ ls
</code><code>
gog_multiwinia_2.0.0.5.sh
play-anything.sh
play-multiwinia.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-multiwinia.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

  * [[https://www.introversion.co.uk/multiwinia/|official website]]
  * [[https://en.wikipedia.org/wiki/Multiwinia|Wikipedia article]]
