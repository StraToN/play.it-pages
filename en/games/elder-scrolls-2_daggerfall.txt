====== The Elder Scrolls II: Daggerfall ======

===== Description =====

[[https://www.dotslashplay.it/images/games/daggerfall/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/daggerfall/thumbnail.jpg?nocache }}]]

**The Elder Scrolls II: Daggerfall**: The ancient golem Numidium, a powerful weapon once used by the great Tiber Septim to unify Tamriel, has been found in Iliac Bay. In the power struggle that follows, the King of Daggerfall is murdered and his spirit haunts the kingdom. The Emperor Uriel Septim VII sends his champion to the province of High Rock to put the king’s spirit to rest and ensure that the golem does not fall into the wrong hands.

  * age: 18y+
  * category: RPG, action, aventure
  * official url: [[https://elderscrolls.bethesda.net/en/daggerfall|]]
  * release date: 1996

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-daggerfall.sh|play-daggerfall.sh]] (updated on 23/05/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/the_elder_scrolls_iii_morrowind_goty_edition|setup_tes_daggerfall_2.0.0.4.exe]] (MD5: 68f1eb4f257d8da4c4eab2104770c49b)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will run through [[https://www.dosbox.com/|DOSBox]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt -S icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-daggerfall.sh
setup_tes_daggerfall_2.0.0.4.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-daggerfall.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Links ====

  * [[https://en.wikipedia.org/wiki/The_Elder_Scrolls_II:_Daggerfall|Wikipedia article]]
