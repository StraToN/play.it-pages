====== Dex ======

===== Description =====

[[https://www.dotslashplay.it/images/games/dex/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/dex/thumbnail.jpg?nocache }}]]

**Dex**: A mysterious, large-scale organization wants you dead and you must cross the futuristic city of Harbor Prime in search of unexpected allies to bring down the system!

  * age: 17y+
  * category: RPG, Action, Science-Fiction
  * official url: [[http://en.dreadlocks.cz/games/dex/|]]
  * release date: 2015

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-dex.sh|play-dex.sh]] (updated on 8/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/dex|dex_en_6_0_0_0_build_5553_17130.sh]] (MD5: 3d6f8797fab72dcb867c92bf5a84b4dd)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
dex_en_6_0_0_0_build_5553_17130.sh
libplayit2.sh
play-dex.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-dex.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
