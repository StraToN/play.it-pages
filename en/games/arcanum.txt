====== Arcanum: Of Steamworks and Magick Obscura ======

===== Description =====

[[https://www.dotslashplay.it/images/games/arcanum/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/arcanum/thumbnail.jpg?nocache }}]]

**Arcanum**: Choose magic or technology, and fight monsters, in either real time or turn-based fights.

  * age: 12y+
  * category: RPG
  * release date: 2001

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-arcanum.sh|play-arcanum.sh]] (updated on 31/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * setup_arcanum_2.0.0.15.exe (MD5: c09523c61edd18abb97da97463e07a88)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-arcanum.sh
setup_arcanum_2.0.0.15.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-arcanum.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
