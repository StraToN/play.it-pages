====== Darwinia ======

===== Description =====

[[https://www.dotslashplay.it/images/games/darwinia/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/darwinia/thumbnail.jpg?nocache }}]]

**Darwinia** is a kind of virtual amusement park populated by Darwinians, thinking entities capable of evolving.\\
Unfortunately, Darwinia has been invaded by a computer virus that has multiplied and threatens to exterminate Darwinians.\\
You must do everything you can to destroy the virus and save the Darwinians from extinction.

  * age: 7y+
  * category: RTS
  * official url: [[https://www.introversion.co.uk/darwinia/|]]
  * release date: 2005

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-darwinia.sh|play-darwinia.sh]] (updated on 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/darwinia|gog_darwinia_2.0.0.5.sh]] (MD5: ef55064ab82a64324e295f2ea96239d6)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_darwinia_2.0.0.5.sh
libplayit2.sh
play-darwinia.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-darwinia.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Links ====

  * [[https://www.introversion.co.uk/darwinia/|official website]]
  * [[https://en.wikipedia.org/wiki/Darwinia_(video_game)|Wikipedia article]]
