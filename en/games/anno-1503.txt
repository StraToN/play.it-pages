====== Anno 1503 ======

===== Description =====

**Anno 1503** is a strategy game that takes place during the period of American colonization.\\
You are a settler determined to conquer new horizons and build your own empire.\\
Four different climates are available on the many islands.

  * age: 3y+
  * category: strategy, management
  * release date: 2003

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-anno-1503.sh|play-anno-1503.sh]] (updated on 8/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/anno_1503_ad|setup_anno_1503_2.0.0.5.exe]] (MD5: a7b6aeb2c5f96e2fab12d1ef12f3b4af)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-anno-1503.sh
setup_anno_1503_2.0.0.5.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-anno-1503.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
