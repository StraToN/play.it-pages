====== Pony Island ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-pony-island.sh|play-pony-island.sh]] (updated on 17/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.humblebundle.com/store/pony-island|ponyisland_linux_v1.20.zip]] (MD5: 30c4f063e360de53cdb6a945df82cca4)
  * dependencies:
    * Arch Linux:
      * unzip
    * Debian:
      * fakeroot
      * unzip

[[https://www.dotslashplay.it/images/games/pony-island/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/pony-island/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S unzip
</code>
    - Debian:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
ponyisland_linux_v1.20.zip
libplayit2.sh
play-pony-island.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-pony-island.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
