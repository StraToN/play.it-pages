====== Icewind Dale II ======

===== Description =====

[[https://www.dotslashplay.it/images/games/icewind-dale-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/icewind-dale-2/thumbnail.jpg?nocache }}]]

**Icewind Dale II**: The greatest fear of civilized kingdoms has become reality.\\
The Goblinoids have united to assemble a great army of scoundrels and outcasts, and now they want to seize the Ten Cities.\\
Swarms of Orcs and Goblins on Warg’s back besiege the city of Targos, and this is only the beginning!\\
A call to arms was launched to all heroes willing to risk everything to defend the Ten Cities.\\
Will you dare to respond and face the greatest threat ever known north of the world’s backbone?

  * age: 12y+
  * category: Adventure, Fantasy, RPG, Dungeons & Dragons
  * release date: 2002

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-icewind-dale-2.sh|play-icewind-dale-2.sh]] (updated on 3/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * English version:
      * [[https://www.gog.com/game/icewind_dale_2|setup_icewind_dale2_2.1.0.13.exe]] (MD5: 9a68fdabdaff58bebc67092d47d4174e)
    * French version:
      * [[https://www.gog.com/game/icewind_dale_2|setup_icewind_dale2_french_2.1.0.13.exe]] (MD5: 04f25433d405671a8975be6540dd55fa)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:
    - English version:<code>
$ ls
</code><code>
libplayit2.sh
play-icewind-dale-2.sh
setup_icewind_dale2_2.1.0.13.exe
</code>
    - French version:<code>
$ ls
</code><code>
libplayit2.sh
play-icewind-dale-2.sh
setup_icewind_dale2_french_2.1.0.13.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-icewind-dale-2.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
