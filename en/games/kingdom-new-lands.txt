====== Kingdom New Lands ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-kingdom-new-lands.sh|play-kingdom-new-lands.sh]] (updated on 30/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/kingdom_new_lands|gog_kingdom_new_lands_2.6.0.8.sh]] (MD5: 0d662366f75d5da214e259d792e720eb)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/kingdom-new-lands/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/kingdom-new-lands/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_kingdom_new_lands_2.6.0.8.sh
libplayit2.sh
play-kingdom-new-lands.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-kingdom-new-lands.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
