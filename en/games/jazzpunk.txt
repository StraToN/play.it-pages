====== Jazzpunk ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-jazzpunk.sh|play-jazzpunk.sh]] (updated on 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * Jazzpunk-Oct-30-2017-Linux.zip (MD5: e8ecf692ded05cea80701d417fa565c1)
    * [[https://www.dotslashplay.it/ressources/jazzpunk/|jazzpunk_icons.tar.gz]] (MD5: d1fe700322ad08f9ac3dec1c29512f94)
  * dependencies:
    * Arch Linux:
      * unzip
    * Debian:
      * fakeroot
      * unzip

[[https://www.dotslashplay.it/images/games/jazzpunk/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/jazzpunk/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S unzip
</code>
    - Debian:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
jazzpunk_icons.tar.gz
Jazzpunk-Oct-30-2017-Linux.zip
libplayit2.sh
play-jazzpunk.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-jazzpunk.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
