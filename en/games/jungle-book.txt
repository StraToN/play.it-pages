====== The Jungle Book ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-the-jungle-book.sh|play-the-jungle-book.sh]] (updated on 28/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/disney_the_jungle_book|gog_disney_s_the_jungle_book_2.0.0.2.sh]] (MD5: bcb57f4ff5cb1662ba3d4a9e34f263ad)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/the-jungle-book/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/the-jungle-book/thumbnail.jpg?nocache }}]]

<note>The game installed via these scripts will run through [[https://www.dosbox.com/|DOSBox]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_disney_s_the_jungle_book_2.0.0.2.sh
libplayit2.sh
play-the-jungle-book.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-the-jungle-book.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Links ====

  * [[https://en.wikipedia.org/wiki/The_Jungle_Book_(video_game)|Wikipedia article]]
