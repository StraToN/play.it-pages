====== Goblins Quest 3 ======

===== Description =====

[[https://www.dotslashplay.it/images/games/goblins-3/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/goblins-3/thumbnail.jpg?nocache }}]]

**Goblins 3**: Blount is a wacky reporter-journalist from "Goblins News" going to make a report in the Foliandre kingdom, where Queen Xina and King Bodd are waging war to possess an infernal labyrinth.\\
However, Blount’s flying nave was attacked and he was forced to descend during his journey. Meeting Princess Winona, daughter of the keeper of the labyrinth, he falls in love with her and finds himself intervening in this conflict.\\
But he also meets a werewolf…

  * category: Adventure
  * release date: 1993

===== Informations =====

//version sold on GOG//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-goblins-3_gog-2.1.0.64.sh|play-goblins-3_gog-2.1.0.64.sh]] (updated on 5/04/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
  * target:
    * English version:
      * setup_gobliiins3_2.1.0.64.exe (MD5: d5d287d784a33ec5fed9372ba451e25a)
    * French version:
      * setup_gobliiins3_french_2.1.0.64.exe (MD5: e830430896de19f11d7cfcc92c5669b9)
  * dependencies:
    * fakeroot
    * innoextract

<note>The game installed via these scripts will run through [[https://www.scummvm.org/|ScummVM]].</note>

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot innoextract
</code>
  - Put in a same directory the scripts and installer:
    - English version:<code>
$ ls
</code><code>
play-anything.sh
play-goblins-3_gog-2.1.0.64.sh
setup_gobliiins3_2.1.0.64.exe
</code>
    - French version:<code>
$ ls
</code><code>
play-anything.sh
play-goblins-3_gog-2.1.0.64.sh
setup_gobliiins3_french_2.1.0.64.exe
</code>
  - Run the building process from this directory:<code>
$ sh ./play-goblins-3_gog-2.1.0.64.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links for more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

  * [[https://en.wikipedia.org/wiki/Gobliiins|Gobliiins series on Wikipedia]]
