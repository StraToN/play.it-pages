====== The Little Acre ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-the-little-acre.sh|play-the-little-acre.sh]] (updated on 31/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/the_little_acre|the_little_acre_en_gog_3_14723.sh]] (MD5: ef9dc3c9600bee4dbf64b29d46b718c6)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/the-little-acre/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/the-little-acre/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-the-little-acre.sh
the_little_acre_en_gog_3_14723.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-the-little-acre.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
