====== Braveland Pirate ======

===== Description =====

[[https://www.dotslashplay.it/images/games/braveland-pirate/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/braveland-pirate/thumbnail.jpg?nocache }}]]

**Braveland Pirate**: The Braveland trilogy concludes with the most exciting book of all, number three – welcome to Braveland Pirate strategy game!\\
A crew of pirates led by Captain Jim embarks on a search for the Eternal Treasure.\\
Hordes of undead, chests full of gold, devious pirate captains, and breathtaking adventures await you in the Free Islands.

  * category: adventure, RPG, strategy, turn-based
  * official url: [[http://www.tortugateam.com/en/|]]
  * release date: 2015

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-braveland-pirate.sh|play-braveland-pirate.sh]] (updated on 8/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/braveland_pirate|braveland_pirate_en_1_1_0_9_18418.sh]] (MD5: 445b705cc11b58fd289064aa78ef9210)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
braveland_pirate_en_1_1_0_9_18418.sh
libplayit2.sh
play-braveland-pirate.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-braveland-pirate.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
