====== Don’t Starve ======

===== Description =====

[[https://www.dotslashplay.it/images/games/dont-starve/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/dont-starve/thumbnail.jpg?nocache }}]]

**Don’t Starve** is an uncompromising wilderness survival game full of science and magic.\\
You play as Wilson, an intrepid Gentleman Scientist who has been trapped by a demon and transported to a mysterious wilderness world. Wilson must learn to exploit his environment and its inhabitants if he ever hopes to escape and find his way back home.\\
Enter a strange and unexplored world full of strange creatures, dangers, and surprises. Gather resources to craft items and structures that match your survival style. Play your way as you unravel the mysteries of this strange land, in this challenging and unnerving wilderness survival sandbox game.

  * category: adventure, survival
  * official url: [[https://www.klei.com/games/dont-starve|]]
  * release date: 2013

===== Informations =====

=== Don’t Starve ===

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-dont-starve.sh|play-dont-starve.sh]] (updated on 6/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/dont_starve|don_t_starve_en_20171215_17629.sh]] (MD5: f7dda3b3bdb15ac62acb212a89b24623)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

=== Reign of Giants ===

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-dont-starve-reign-of-giants.sh|play-dont-starve-reign-of-giants.sh]] (updated on 17/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/dont_starve_reign_of_giants|don_t_starve_reign_of_giants_dlc_en_20171215_17628.sh]] (MD5: 47084ab8d5b36437e1bcb899c35bfe00)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

=== Shipwrecked ===

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-dont-starve-shipwrecked.sh|play-dont-starve-shipwrecked.sh]] (updated on 17/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/dont_starve_shipwrecked|don_t_starve_shipwrecked_dlc_en_20171215_17628.sh]] (MD5: 463825173d76f294337f0ae7043d7cf6)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage (Don’t Starve) ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
don_t_starve_en_20171215_17629.sh
libplayit2.sh
play-dont-starve.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-dont-starve.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Usage (Reign of Giants) ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
don_t_starve_reign_of_giants_dlc_en_20171215_17628.sh
libplayit2.sh
play-dont-starve-reign-of-giants.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-dont-starve-reign-of-giants.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Usage (Shipwrecked) ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
don_t_starve_shipwrecked_dlc_en_20171215_17628.sh
libplayit2.sh
play-dont-starve-shipwrecked.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-dont-starve-shipwrecked.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Mods management ====

You can install mods simply by putting them in the following directory:
<code>~/.local/share/games/dont-starve/mods</code>
