====== Stardew Valley ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-stardew-valley.sh|play-stardew-valley.sh]] (updated on 4/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/stardew_valley|gog_stardew_valley_2.8.0.10.sh]] (MD5: 27c84537bee1baae4e3c2f034cb0ff2d)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/stardew-valley/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/stardew-valley/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-stardew-valley.sh
gog_stardew_valley_2.8.0.10.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-stardew-valley.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
