====== Memoranda ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-memoranda.sh|play-memoranda.sh]] (updated on 3/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * [[https://www.gog.com/game/memoranda|gog_memoranda_2.2.0.3.sh]] (MD5: 9671ebb592d4b4a028fd80f76e96c1a1)
    * [[https://www.dotslashplay.it/ressources/libssl/|libssl_1.0.0_32-bit.tar.gz]] (MD5: 9443cad4a640b2512920495eaf7582c4)
  * dependencies:
    * Arch Linux:
      * imagemagick
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot
      * imagemagick

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S imagemagick libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot imagemagick
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_memoranda_2.2.0.3.sh
libplayit2.sh
libssl_1.0.0_32-bit.tar.gz
play-memoranda.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-memoranda.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
