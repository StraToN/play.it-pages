====== Battle Worlds: Kronos ======

===== Description =====

[[https://www.dotslashplay.it/images/games/battle-worlds-kronos/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/battle-worlds-kronos/thumbnail.jpg?nocache }}]]

**Battle Worlds: Kronos**: Experience the two sides of a battle while making use of multiple combat styles, from dominating a civilization with epic tactical warfare, to saving your planet from superior military forces with clever guerilla tactics.

  * age: 12y+
  * category: strategy, turn-based
  * official url: [[https://kingart-games.com/games/1-battle-worlds-kronos/|]]
  * release date: 2016

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-battle-worlds-kronos.sh|play-battle-worlds-kronos.sh]] (updated on 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/battle_worlds_kronos|gog_battle_worlds_kronos_2.1.0.6.sh]] (MD5: 4d7f9e05524ff6b85139df903dbcb74b)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_battle_worlds_kronos_2.1.0.6.sh
libplayit2.sh
play-battle-worlds-kronos.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-battle-worlds-kronos.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>

==== Links ====

  * [[http://www.battle-worlds.com/|official website]]
