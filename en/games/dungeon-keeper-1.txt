====== Dungeon Keeper Gold ======

===== Description =====

[[https://www.dotslashplay.it/images/games/dungeon-keeper-1/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/dungeon-keeper-1/thumbnail.jpg?nocache }}]]

**Dungeon Keeper Gold**: As keeper of the dungeon, you’re here at home.\\
And it’s your job to turn these disgusting creatures of darkness into destructive soldiers, screaming, drooling and claws outside.\\
To attract them, promise them food and a fetid, damp place to sleep.\\
To get them to obey, use the whip and make them understand the dangers of revolting.\\
Then, send your scaly monsters to confront the best defenders of the good. They will not hesitate to die in battle, eviscerating the unfortunate heroes who dare to stand in their way.

  * category: strategy, action, fantasy
  * release date: 1997

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-dungeon-keeper-1.sh|play-dungeon-keeper-1.sh]] (updated on 2/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/dungeon_keeper|setup_dungeon_keeper_gold_2.1.0.7.exe]] (MD5: 8f8890d743c171fb341c9d9c87c52343)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
      * unarchiver
    * Debian:
      * fakeroot
      * icoutils
      * innoextract
      * unar

<note>The game installed via these scripts will run through [[https://www.dosbox.com/|DOSBox]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract unarchiver
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract unar
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-dungeon-keeper-1.sh
setup_dungeon_keeper_gold_2.1.0.7.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-dungeon-keeper-1.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
