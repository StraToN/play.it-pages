====== Shadow Tactics: Blades of the Shogun ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-shadow-tactics.sh|play-shadow-tactics.sh]] (updated on 20/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/shadow_tactics_blades_of_the_shogun|shadow_tactics_blades_of_the_shogun_en_1_4_4_f_14723.sh]] (MD5: 93faa090d5bcaa22f0faabd1e32c5909)
  * dependencies:
    * Arch Linux:
      * unzip
    * Debian:
      * fakeroot
      * unzip

[[https://www.dotslashplay.it/images/games/shadow-tactics/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/shadow-tactics/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S unzip
</code>
    - Debian:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-shadow-tactics.sh
shadow_tactics_blades_of_the_shogun_en_1_4_4_f_14723.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-shadow-tactics.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
