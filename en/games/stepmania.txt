====== StepMania ======
//version downloadable from StepMania.com//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-stepmania_5.0.10.sh|play-stepmania_5.0.10.sh]]
  * target:
    * [[http://www.stepmania.com/download/|StepMania-5.0.10-Linux.tar.gz]]
  * dependencies:
    * fakeroot
    * icoutils

[[https://www.dotslashplay.it/images/games/stepmania/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/stepmania/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot icoutils
</code>
  - Download and put in a same directory the scripts and installer:<code>
$ ls
</code><code>
play-anything.sh
play-stepmania_5.0.10.sh
StepMania-5.0.10-Linux.tar.gz
</code>
  - Run the building process from this directory:<code>
$ sh ./play-stepmania_5.0.10.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game.

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

[[http://www.stepmania.com/|official website]]\\
[[https://en.wikipedia.org/wiki/StepMania|StepMania on Wikipedia]]

=== Debian ===

[[https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=200715|discussion about an official package for StepMania]]\\
[[https://github.com/emillon/stepmania-debian|git repository for the Debian package]]
