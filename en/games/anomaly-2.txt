====== Anomaly 2 ======

===== Description =====

[[https://www.dotslashplay.it/images/games/anomaly-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/anomaly-2/thumbnail.jpg?nocache }}]]

**Anomaly 2**: Following the invasion of Earth in 2018, the blue planet is now under the control of alien machines.\\
Humanity is then doomed to disappear. Organized in huge convoys, the survivors roam the tundra in search of food and provisions.\\
Since the war, the roles have been reversed: henceforth, our species seems to be the anomaly on a planet controlled by machines.\\
In this apocalyptic context, you find yourself at the head of the Yukon convoy.

  * category: strategy, action, tower defense
  * official url: [[http://www.anomalythegame.com/#A2]]
  * release date: 2013

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-anomaly-2.sh|play-anomaly-2.sh]] (updated on 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * [[https://www.humblebundle.com/store/anomaly-2|Anomaly2_Linux_1387299615.zip]] (MD5: 46f0ecd5363106e9eae8642836c29dfc)
    * [[https://www.dotslashplay.it/ressources/anomaly-2/|anomaly-2_icons.tar.gz]] (MD5: 73ddbd1651e08d6c8bb4735e5e0a4a81)
  * dependencies:
    * Arch Linux:
      * unzip
    * Debian:
      * fakeroot
      * unzip

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S unzip
</code>
    - Debian:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
anomaly-2_icons.tar.gz
Anomaly2_Linux_1387299615.zip
libplayit2.sh
play-anomaly-2.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-anomaly-2.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
