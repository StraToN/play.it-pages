====== Risk of Rain ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-risk-of-rain.sh|play-risk-of-rain.sh]] (updated on 14/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * GOG:
      * [[https://www.gog.com/game/risk_of_rain|gog_risk_of_rain_2.1.0.5.sh]] (MD5: 34f8e1e2dddc6726a18c50b27c717468)
      * [[https://www.dotslashplay.it/ressources/libssl/|libssl_1.0.0_32-bit.tar.gz]] (MD5: 9443cad4a640b2512920495eaf7582c4)
    * Humble Bundle:
      * [[https://www.humblebundle.com/store/risk-of-rain|Risk_of_Rain_v1.3.0_DRM-Free_Linux_.zip]] (MD5: 21eb80a7b517d302478c4f86dd5ea9a2)
      * [[https://www.dotslashplay.it/ressources/libssl/|libssl_1.0.0_32-bit.tar.gz]] (MD5: 9443cad4a640b2512920495eaf7582c4)
  * dependencies:
    * GOG:
      * Arch Linux:
        * libarchive
      * Debian:
        * bsdtar
        * fakeroot
    * Humble Bundle:
      * Arch Linux:
        * unzip
      * Debian:
        * fakeroot
        * unzip

[[https://www.dotslashplay.it/images/games/risk-of-rain/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/risk-of-rain/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - GOG:
      - Arch Linux:<code>
# pacman -S libarchive
</code>
      - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
    - Humble Bundle:
      - Arch Linux:<code>
# pacman -S unzip
</code>
      - Debian:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:
    - GOG:<code>
$ ls
</code><code>
gog_risk_of_rain_2.1.0.5.sh
libplayit2.sh
libssl_1.0.0_32-bit.tar.gz
play-risk-of-rain.sh
</code>
    - Humble Bundle:<code>
$ ls
</code><code>
libplayit2.sh
libssl_1.0.0_32-bit.tar.gz
play-risk-of-rain.sh
Risk_of_Rain_v1.3.0_DRM-Free_Linux_.zip
</code>
  - Start the building process from this directory:<code>
$ sh ./play-risk-of-rain.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
