====== Aquaria ======

===== Description =====

[[https://www.dotslashplay.it/images/games/aquaria/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/aquaria/thumbnail.jpg?nocache }}]]

**Aquaria** is massive ocean world, teeming with life and filled with ancient secrets.\\
Join Naija, a lone underwater dweller in search of her family, as she explores the depths of Aquaria.\\
She’ll travel from hidden caves, shrouded in darkness, to beautiful, sunlit oases…

  * category: adventure, brainstorm
  * official url: [[http://www.bit-blot.com/aquaria/|]]
  * release date: 2007

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-aquaria.sh|play-aquaria.sh]] (updated on 10/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/aquaria|gog_aquaria_2.0.0.5.sh]] (MD5: 4235398debdf268f233881fade9e0530)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_aquaria_2.0.0.5.sh
libplayit2.sh
play-aquaria.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-aquaria.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
