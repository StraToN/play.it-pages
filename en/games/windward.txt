====== Windward ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-windward.sh|play-windward.sh]] (updated on 25/09/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * GOG:
      * [[https://www.gog.com/game/windward|gog_windward_2.36.0.40.sh]] (MD5: 6afbdcfda32a6315139080822c30396a)
    * Humble Bundle:
      * WindwardLinux_HB_1505248588.zip (MD5: 9ea99157d13ae53905757f2fb3ab5b54)
  * dependencies:
    * Arch Linux:
      * GOG:
        * libarchive
      * Humble Bundle:
        * unzip
    * Debian:
      * GOG:
        * bsdtar
        * fakeroot
      * Humble Bundle:
        * fakeroot
        * unzip

[[https://www.dotslashplay.it/images/games/windward/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/windward/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:
      - GOG:<code>
# pacman -S libarchive
</code>
      - Humble Bundle:<code>
# pacman -S unzip
</code>
    - Debian:
      - GOG:<code>
# apt-get install bsdtar fakeroot
</code>
      - Humble Bundle:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:
    - GOG:<code>
$ ls
</code><code>
gog_windward_2.36.0.40.sh
libplayit2.sh
play-windward.sh
</code>
    - Humble Bundle:<code>
$ ls
</code><code>
libplayit2.sh
play-windward.sh
WindwardLinux_HB_1505248588.zip
</code>
  - Start the building process from this directory:<code>
$ sh ./play-windward.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
