====== Door Kickers ======

===== Description =====

[[https://www.dotslashplay.it/images/games/door-kickers/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/door-kickers/thumbnail.jpg?nocache }}]]

**Door Kickers**: You are in charge of a SWAT squad during various operations.\\
Take a good look at the situation, plan your team's movements, choose the equipment of its members, decide on the entry points and coordinate all this small world to reach the hostages before the criminals execute them.

  * category: RTS, Tactic
  * official url: [[http://inthekillhouse.com/doorkickers/|]]
  * release date: 2014

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-door-kickers.sh|play-door-kickers.sh]] (updated on 17/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * GOG:
      * [[https://www.gog.com/game/door-kickers|gog_door-kickers_2.9.0.12.sh]] (MD5: 29efa58e4a61060b0b1211dddd2476a1)
    * Humble Bundle:
      * [[https://www.humblebundle.com/store/door-kickers|door-kickers_01_08_Linux.zip]] (MD5: 0126db31867ae0e7a7eceee54de4a177)
  * dependencies:
    * GOG:
      * Arch Linux:
        * libarchive
      * Debian:
        * bsdtar
        * fakeroot
    * Humble Bundle:
      * Debian:
        * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - GOG:
      - Arch Linux:<code>
# pacman -S libarchive
</code>
      - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
    - Humble Bundle:
      - Debian:<code>
# apt-get install fakeroot
</code>
  - Put in a same directory the scripts and archive:
    - GOG:<code>
$ ls
</code><code>
gog_door-kickers_2.9.0.12.sh
libplayit2.sh
play-door-kickers.sh
</code>
    - Humble Bundle:<code>
$ ls
</code><code>
door-kickers_01_08_Linux.zip
libplayit2.sh
play-door-kickers.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-door-kickers.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
