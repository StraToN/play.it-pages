====== Lumo ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-lumo.sh|play-lumo.sh]] (updated on 24/09/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/lumo|gog_lumo_2.3.0.4.sh]] (MD5: 76cf720f68446eca8d89dc58cc9c4f7b)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/lumo/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/lumo/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_lumo_2.3.0.4.sh
libplayit2.sh
play-lumo.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-lumo.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
