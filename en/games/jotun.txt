====== Jotun ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-jotun.sh|play-jotun.sh]] (updated on 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/jotun|gog_jotun_2.3.0.5.sh]] (MD5: e79a13252802fe4fe008e817aa2d4f43)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/jotun/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/jotun/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_jotun_2.3.0.5.sh
libplayit2.sh
play-jotun.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-jotun.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
