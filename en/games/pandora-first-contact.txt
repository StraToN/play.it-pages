====== Pandora: First Contact ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-pandora-first-contact.sh|play-pandora-first-contact.sh]] (updated on 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/pandora|pandora_first_contact_en_1_6_7_16815.sh]] (MD5: 0d9343d1693fc561823811a0cd3e279c)
    * optional: [[https://www.dotslashplay.it/ressources/pandora-first-contact/|pandora-first-contact_icons.tar.gz]] (MD5: 66b1d99166b738b2130449a49b9cd58c)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/pandora-first-contact/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/pandora-first-contact/thumbnail.jpg?nocache }}]]

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-pandora-first-contact.sh
pandora_first_contact_en_1_6_7_16815.sh
pandora-first-contact_icons.tar.gz
</code>
  - Start the building process from this directory:<code>
$ sh ./play-pandora-first-contact.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
