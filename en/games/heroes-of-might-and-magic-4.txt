====== Heroes of Might and Magic 4 ======

===== Description =====

[[https://www.dotslashplay.it/images/games/heroes-of-might-and-magic-4/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/heroes-of-might-and-magic-4/thumbnail.jpg?nocache }}]]

**HoMM 4**: Following the destruction of the world of Erathia, refugees have fled through a magic portal to a new world, Axeoth, waiting to be divided and conquered.

  * age: 12y+
  * category: Strategy, Turn-based, Fantasy
  * release date: 2002

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-heroes-of-might-and-magic-4.sh|play-heroes-of-might-and-magic-4.sh]] (updated on 6/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * English version:
      * [[https://www.gog.com/game/heroes_of_might_and_magic_4_complete|setup_homm4_complete_2.0.0.12.exe]] (MD5: 74de66eb408bb2916dd0227781ba96dc)
    * French version:
      * [[https://www.gog.com/game/heroes_of_might_and_magic_4_complete|setup_homm4_complete_french_2.1.0.14.exe]] (MD5: 2af96eb28226e563bbbcd62771f3a319)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:
    - English version:<code>
$ ls
</code><code>
libplayit2.sh
play-heroes-of-might-and-magic-4.sh
setup_homm4_complete_2.0.0.12.exe
</code>
    - French version:<code>
$ ls
</code><code>
libplayit2.sh
play-heroes-of-might-and-magic-4.sh
setup_homm4_complete_french_2.1.0.14.exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-heroes-of-might-and-magic-4.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
