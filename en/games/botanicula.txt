====== Botanicula ======

===== Description =====

[[https://www.dotslashplay.it/images/games/botanicula/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/botanicula/thumbnail.jpg?nocache }}]]

**Botanicula**: In the skin (the bark?) of a group of five characters grappling with a group of spiders parasitizing their tree, you will have to do your best to save your last seed and thus ensure the perenniality of your species.

  * age: 3y+
  * category: adventure, Point’n Click
  * official url: [[http://botanicula.net/|]]
  * release date: 2012

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-botanicula.sh|play-botanicula.sh]] (updated on 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/botanicula|gog_botanicula_2.0.0.2.sh]] (MD5: 7b92a379f8d2749e2f97c43ecc540c3c)
  * dependencies:
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S libarchive
</code>
    - Debian:<code>
# apt-get install bsdtar fakeroot
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
gog_botanicula_2.0.0.2.sh
libplayit2.sh
play-botanicula.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-botanicula.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
