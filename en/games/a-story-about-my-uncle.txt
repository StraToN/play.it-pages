====== A Story About My Uncle ======

===== Description =====

[[https://www.dotslashplay.it/images/games/a-story-about-my-uncle/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/a-story-about-my-uncle/thumbnail.jpg?nocache }}]]

**A Story About My Uncle** is a story about a boy who searches for his lost uncle and ends up in a world he couldn’t imagine existed.

  * age: 12y+
  * category: platform, adventure, first-person perspective
  * official url: [[http://gonenorthgames.com/games/a-story-about-my-uncle/|]]
  * release date: 2014

===== Informations =====

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-a-story-about-my-uncle.sh|play-a-story-about-my-uncle.sh]] (updated on 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * Linux-NoDRM-ASAMU_5188.zip (MD5: 71f9f3add29a733c4a1a7d18d738d3d6)
    * [[https://www.dotslashplay.it/ressources/a-story-about-my-uncle/|a-story-about-my-uncle_icons.tar.gz]] (MD5: db4eb7ab666e61ea5fc983102099ab31)
  * dependencies:
    * Arch Linux:
      * unzip
    * Debian:
      * fakeroot
      * unzip

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S unzip
</code>
    - Debian:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
a-story-about-my-uncle_icons.tar.gz
libplayit2.sh
Linux-NoDRM-ASAMU_5188.zip
play-a-story-about-my-uncle.sh
</code>
  - Start the building process from this directory:<code>
$ sh ./play-a-story-about-my-uncle.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
