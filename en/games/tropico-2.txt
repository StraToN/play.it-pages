====== Tropico 2: Pirate Cove ======
//version sold on GOG//

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (updated on 18/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-tropico-2_gog-2.1.0.14.sh|play-tropico-2_gog-2.1.0.14.sh]]
  * target:
    * setup_tropico2_2.1.0.14.exe or setup_tropico2_french_2.1.0.14.exe
  * dependencies:
    * fakeroot
    * icoutils
    * innoextract

[[https://www.dotslashplay.it/images/games/tropico-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/tropico-2/thumbnail.jpg?nocache }}]]

<note>The game installed via these scripts will run through [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Download and put in a same directory the scripts and installer:<code>
$ ls
</code><code>
play-anything.sh
play-tropico-2_gog-2.1.0.14.sh
setup_tropico2_2.1.0.14.exe
</code>
  - Run the building process from this directory:<code>
$ sh ./play-tropico-2_gog-2.1.0.14.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game.

==== Advanced usage ====

This script accept several options to set its behaviour. Follow the links to more details on how to use them.

  * [[:en:common:advanced-options#checksum|checksum]]
  * [[:en:common:advanced-options#compression|compression]]
  * [[:en:common:advanced-options#help|help]]
  * [[:en:common:advanced-options#prefix|prefix]]

==== Links ====

[[https://en.wikipedia.org/wiki/Tropico_2:_Pirate_Cove|Tropico 2 on Wikipedia]]
