====== Dreaming Sarah ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-dreaming-sarah.sh|play-dreaming-sarah.sh]] (updated on 10/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * targets:
    * 32-bit:
      * [[https://www.humblebundle.com/store/dreaming-sarah|DreamingSarah-linux32_1.3.zip]] (MD5: 73682a545e979ad9a2b6123222ddb517)
    * 64-bit:
      * [[https://www.humblebundle.com/store/dreaming-sarah|DreamingSarah-linux64_1.3.zip]] (MD5: a68f3956eb09ea7b34caa20f6e89b60c)
  * dependencies:
    * Arch Linux:
      * unzip
    * Debian:
      * fakeroot
      * unzip

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
# pacman -S unzip
</code>
    - Debian:<code>
# apt-get install fakeroot unzip
</code>
  - Put in a same directory the scripts and archive:
    - 32-bit:<code>
$ ls
</code><code>
libplayit2.sh
play-dreaming-sarah.sh
DreamingSarah-linux32_1.3.zip
</code>
    - 64-bit:<code>
$ ls
</code><code>
libplayit2.sh
play-dreaming-sarah.sh
DreamingSarah-linux64_1.3.zip
</code>
  - Start the building process from this directory:<code>
$ sh ./play-regency-solitaire.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
