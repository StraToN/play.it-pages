====== Monkey Island 3: The Curse of Monkey Island ======

  * scripts:
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-monkey-island-3.sh|play-monkey-island-3.sh]] (updated on 1/04/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (updated on 2/04/2018)
  * target:
    * [[https://www.gog.com/game/the_curse_of_monkey_island|setup_the_curse_of_monkey_island_1.0_(18253).exe]] (MD5: 20c74e5f60bd724182ec2bdbae6d9a49)
  * dependencies:
    * Arch Linux:
      * icoutils
      * innoextract
    * Debian:
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/monkey-island-3/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/monkey-island-3/thumbnail.jpg?nocache }}]]

<note>The game installed via these scripts will use [[https://www.winehq.org/|WINE]].</note>

==== Usage ====

  - Install the scripts dependencies:
    - Arch Linux:<code>
$ yaourt icoutils innoextract
</code>
    - Debian:<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Put in a same directory the scripts and archive:<code>
$ ls
</code><code>
libplayit2.sh
play-monkey-island-3.sh
setup_the_curse_of_monkey_island_1.0_(18253).exe
</code>
  - Start the building process from this directory:<code>
$ sh ./play-monkey-island-3.sh
</code>
  - Wait a couple minutes, the building will end by giving you the commands to launch as root to install the game. It should be something similar to:
    - Arch Linux:<code>
# pacman -U /some/path/to/game_package.pkg.tar
</code>
    - Debian:<code>
# dpkg -i /some/path/to/game_package.deb
# apt-get install -f
</code>
