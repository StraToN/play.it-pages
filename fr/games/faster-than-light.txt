====== FTL: Faster Than Light ======

===== Description =====

[[https://www.dotslashplay.it/images/games/faster-than-light/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/faster-than-light/thumbnail.jpg?nocache }}]]

**Faster Than Light** : vous vivez l’atmosphère d’un vaisseau spatial qui tente de sauver la galaxie. C’est une mission dangereuse, chaque rencontre présentant un défi unique avec des solutions multiples.

  * catégorie : strategy, simulation, Sci-Fi
  * url officielle : [[https://subsetgames.com/ftl.html|]]
  * date de sortie : 2012

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-faster-than-light.sh|play-faster-than-light.sh]] (mis à jour le 21/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cibles :
    * GOG :
      * [[https://www.gog.com/game/faster_than_light|ftl_advanced_edition_en_1_6_7_18662.sh]] (MD5 : 2c5254547639b7718dac7a06dabd1d82)
    * Humble Bundle :
      * [[https://www.humblebundle.com/store/ftl-faster-than-light|FTL.1.5.13.tar.gz]] (MD5 : 791e0bc8de73fcdcd5f461a4548ea2d8)
  * dépendances :
    * Arch Linux :
      * GOG :
        * imagemagick
        * libarchive
      * Humble Bundle :
        * imagemagick
    * Debian :
      * GOG :
        * bsdtar
        * fakeroot
        * imagemagick
      * Humble Bundle :
        * fakeroot
        * imagemagick

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :
      - GOG :<code>
# pacman -S imagemagick libarchive
</code>
      - Humble Bundle :<code>
# pacman -S imagemagick
</code>
    - Debian :
      - GOG :<code>
# apt-get install bsdtar fakeroot imagemagick
</code>
      - Humble Bundle :<code>
# apt-get install fakeroot imagemagick
</code>
  - Placez dans un même répertoire les scripts et l’archive :
    - GOG :<code>
$ ls
</code><code>
ftl_advanced_edition_en_1_6_7_18662.sh
libplayit2.sh
play-faster-than-light.sh
</code>
    - Humble Bundle :<code>
$ ls
</code><code>
FTL.1.5.13.tar.gz
libplayit2.sh
play-faster-than-light.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-faster-than-light.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
