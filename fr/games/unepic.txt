====== unEpic ======

=== version vendue sur GOG ===

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-unepic_gog-2.1.0.4.sh|play-unepic_gog-2.1.0.4.sh]] (mis à jour le 5/06/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * gog_unepic_2.1.0.4.sh (MD5 : 341556e144d5d17ae23d2b0805c646a1)
  * dépendances :
    * fakeroot
    * unzip

=== version vendue sur Humble Bundle ===

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-unepic_humblebundle-2014-12-08.sh|play-unepic_humblebundle-2014-12-08.sh]]
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * unepic-15005.run (MD5 : 940824c4de6e48522845f63423e87783)
  * dépendances :
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/unepic/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/unepic/thumbnail.jpg?nocache }}]]

==== Utilisation (version GOG) ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
gog_unepic_2.1.0.4.sh
play-anything.sh
play-unepic_gog-2.1.0.4.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-unepic_gog-2.1.0.4.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation (version Humble) ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
play-anything.sh
play-unepic_humblebundle-2014-12-08.sh
unepic-15005.run
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-unepic_humblebundle-2014-12-08.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/Unepic|article Wikipédia]]
