====== Lumo ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-lumo.sh|play-lumo.sh]] (mis à jour le 24/09/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/lumo|gog_lumo_2.3.0.4.sh]] (MD5 : 76cf720f68446eca8d89dc58cc9c4f7b)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/lumo/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/lumo/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_lumo_2.3.0.4.sh
libplayit2.sh
play-lumo.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-lumo.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
