====== Ziggurat ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-ziggurat.sh|play-ziggurat.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * ZigguratLinux.tar.gz (MD5 : f990c99e4351b1ae465d551f0c5030be)
  * dépendances :
    * Debian :
      * fakeroot

[[https://www.dotslashplay.it/images/games/ziggurat/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/ziggurat/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Debian :<code>
# apt-get install fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
ZigguratLinux.tar.gz
libplayit2.sh
play-ziggurat.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-ziggurat.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
