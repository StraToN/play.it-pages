====== Fallout Tactics ======

===== Description =====

[[https://www.dotslashplay.it/images/games/fallout-tactics/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/fallout-tactics/thumbnail.jpg?nocache }}]]

**Fallout Tactics** : la nouvelle Confrérie de l’Acier essaye de s’approprier le territoire entourant Chicago. En offrant leur protection aux villages tribaux, la Confrérie reçoit en échange les meilleures recrues de ces villages. Au début du jeu, le joueur est un Initié, une nouvelle recrue de la Confrérie, qui a pour tâche de commander une escouade composée d'autres Initiés.

  * age : +16 ans
  * catégorie : stratégie
  * date de sortie : 2001

===== Informations =====

//version vendue sur GOG//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-fallout-tactics_gog-2.1.0.12.sh|play-fallout-tactics_gog-2.1.0.12.sh]] (mis à jour le 9/03/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * version anglaise :
      * setup_fallout_tactics_2.1.0.12.exe (MD5: 9cc1d9987d8a2fa6c1cc6cf9837758ad)
    * version française :
      * setup_fallout_tactics_french_2.1.0.12.exe (MD5: 520c29934290910cdeecbc7fcca68f2b)
  * dépendances :
    * fakeroot
    * innoextract
    * icoutils (optionnel)

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
play-anything.sh
play-fallout-tactics_gog-2.1.0.12.sh
setup_fallout_tactics_french_2.1.0.12.exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-fallout-tactics_gog-2.1.0.12.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Configurer le préfixe WINE ====

Vous pouvez accéder à la fenêtre de configuration du préfixe WINE dédié au jeu en lançant la commande suivante :
<code>
bos-winecfg
</code>
Toute modification effectuée via cette fenêtre sera spécifique à ce jeu, sans influence pour vos autres jeux WINE, qu’ils aient été installés via un script ./play.it ou un appel direct à WINE.

==== Lancer le jeu dans une fenêtre ====

La méthode suivante va vous permettre de lancer le jeu en mode fenêtré.
  - Ouvrez la fenêtre de configuration du préfixe WINE, via la commande donnée plus haut ;
  - Dans la fenêtre qui s’affiche, allez sur l’onglet "Affichage" ;
  - Cochez la case "Émuler un bureau virtuel", et dans les champs "Taille du bureau" renseignez la résolution qu’utilise votre bureau ;
  - Fermez la fenêtre avec "OK".
C’est fait, Fallout Tactics se lancera maintenant dans un fenêtre adaptée à la résolution qu’il utilise. Vous pouvez à tout moment revenir à un mode plein écran en retournant dans la fenêtre de configuration de WINE et en décochant la case "Émuler un bureau virtuel".

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/Fallout_Tactics|Fallout Tactics sur Wikipédia]]
  * [[https://appdb.winehq.org/objectManager.php?sClass=version&iId=810|entrée dans la base de donnée WineHQ AppDB]] (en anglais)
