====== Legend of Grimrock ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-legend-of-grimrock.sh|play-legend-of-grimrock.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/legend_of_grimrock|gog_legend_of_grimrock_2.1.0.5.sh]] (MD5 : b63089766247484f5d2b214d924425f6)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/legend-of-grimrock/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/legend-of-grimrock/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_legend_of_grimrock_2.1.0.5.sh
libplayit2.sh
play-legend-of-grimrock.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-legend-of-grimrock.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
