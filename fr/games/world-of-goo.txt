====== World of Goo ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-world-of-goo.sh|play-world-of-goo.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/world_of_goo|gog_world_of_goo_2.0.0.3.sh]] (MD5 : 5359b8e7e9289fba4bcf74cf22856655)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/world-of-goo/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/world-of-goo/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_world_of_goo_2.0.0.3.sh
libplayit2.sh
play-world-of-goo.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-world-of-goo.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/World_of_Goo|article Wikipédia]]
