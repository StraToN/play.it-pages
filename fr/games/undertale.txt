====== Undertale ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-undertale.sh|play-undertale.sh]] (mis à jour le 14/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cibles :
    * [[https://www.gog.com/game/undertale|undertale_en_1_08_18328.sh]] (MD5 : b134d85dd8bf723a74498336894ca723)
    * [[https://www.dotslashplay.it/ressources/libssl/|libssl_1.0.0_32-bit.tar.gz]] (MD5 : 9443cad4a640b2512920495eaf7582c4)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/undertale/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/undertale/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
undertale_en_1_08_18328.sh
libplayit2.sh
libssl_1.0.0_32-bit.tar.gz
play-undertale.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-undertale.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
