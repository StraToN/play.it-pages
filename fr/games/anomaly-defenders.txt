====== Anomaly Defenders ======

===== Description =====

[[https://www.dotslashplay.it/images/games/anomaly-defenders/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/anomaly-defenders/thumbnail.jpg?nocache }}]]

**Anomaly Defenders** est le dernier épisode de la série Anomaly.
L’incarnation originale du sous-genre Tower Offense avait des joueurs contrôlant les humains qui combattaient les envahisseurs étrangers.\\
Maintenant, les tables ont tourné. La contre-attaque humaine est en cours et la planète extraterrestre est menacée.\\
Vous devez défendre la planète contre la racaille humaine dans la bataille finale de la série.

  * catégorie : stratégie, action, tower defense
  * url officielle : [[http://www.anomalythegame.com/#AD|]]
  * date de sortie : 2014

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-anomaly-defenders.sh|play-anomaly-defenders.sh]] (mis à jour le 8/09/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cibles :
    * [[https://www.humblebundle.com/store/anomaly-defenders|AnomalyDefenders_Linux_1402512837.tar.gz]] (MD5 : 35ccd57e8650dd53a09b1f1e088307cc)
  * dépendances :
    * Debian :
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Debian :<code>
# apt-get install fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
AnomalyDefenders_Linux_1402512837.tar.gz
libplayit2.sh
play-anomaly-defenders.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-anomaly-defenders.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
