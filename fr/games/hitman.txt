====== Hitman: Codename 47 ======

===== Description =====

[[https://www.dotslashplay.it/images/games/hitman/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/hitman/thumbnail.jpg?nocache }}]]

**Hitman: Codename 47** :  Pour nous, la société est normale. Nous marchons dans les rues sans craindre personne et rien.\\
Pourtant, à travers ses yeux bleus comme la glace, l'agent 47 voit un monde de menaces et de dangers potentiels.\\
Il regarde les gens intéressés, mais non impliqués. Votre heure est venue d'entrer dans le monde de Hitman.\\
Un monde où vous ne faites confiance qu'à vous-même, un joueur dans un jeu mortel, un guerrier sur un champ de bataille dont la plupart des gens ne connaissent même pas l'existence.

  * age : +18 ans
  * catégorie : action, assassin, infiltration
  * url officielle : [[https://hitman.com|]]
  * date de sortie : 2000

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-hitman.sh|play-hitman.sh]] (mis à jour le 10/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/hitman|setup_hitman_codename_47_b192_(17919).exe]] (MD5 : 018d8191bfa45c16995537e3a93f96bd)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-hitman.sh
setup_hitman_codename_47_b192_(17919).exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-hitman.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
