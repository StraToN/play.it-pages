====== Divine Divinity ======

===== Description =====

[[https://www.dotslashplay.it/images/games/divine-divinity/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/divine-divinity/thumbnail.jpg?nocache }}]]

**Divine Divinity** : Rivellon est sur le point de disparaître…\\
De puissantes forces obscures ont déclaré la guerre au peuple des sept races.\\
Mais il reste une lueur d'espoir : l’ancienne prophétie concernant l’élu peut encore se réaliser, grâce à vous, cher joueur ! 

  * age : +7 ans
  * catégorie : RPG, Fantasy, Action
  * url officielle : [[http://larian.com/games/divine-divinity/|]]
  * date de sortie : 2002

===== Informations =====

//version vendue sur GOG//

  * scripts :
    * version anglaise :
      * [[https://www.dotslashplay.it/scripts/play.it-1/play-divine-divinity_gog-2.0.0.21.sh|play-divine-divinity_gog-2.0.0.21.sh]] (mis à jour le 23/02/2016)
      * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
    * version française :
      * [[https://www.dotslashplay.it/scripts/play.it-1/play-divine-divinity_gog-2.1.0.32.sh|play-divine-divinity_gog-2.1.0.32.sh]] (mis à jour le 23/02/2016)
      * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * version anglaise :
      * setup_divine_divinity_2.0.0.21.exe (MD5 : 3798d48f04a7a8444fd9f4c32b75b41d)
    * version française :
      * setup_divine_divinity_french_2.1.0.32.exe (MD5 : f755d69ad7d319fb70298844dcb3861a)
  * dépendances :
    * fakeroot
    * innoextract
    * icoutils (optionnel)

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation (version anglaise) ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
play-anything.sh
play-divine-divinity_gog-2.0.0.21.sh
setup_divine_divinity_2.0.0.21.exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-divine-divinity_gog-2.0.0.21.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation (version française) ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
play-anything.sh
play-divine-divinity_gog-2.1.0.32.sh
setup_divine_divinity_french_2.1.0.32.exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-divine-divinity_gog-2.1.0.32.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Configurer le préfixe WINE ====

Vous pouvez accéder à la fenêtre de configuration du préfixe WINE dédié au jeu en lançant la commande suivante :
<code>
div1-winecfg
</code>
Toute modification effectuée via cette fenêtre sera spécifique à ce jeu, sans influence pour vos autres jeux WINE, qu’ils aient été installé via un script ./play.it ou un appel direct à WINE.

==== Lancer le jeu dans une fenêtre ====

La méthode suivante va vous permettre de lancer le jeu en mode fenêtré facilement :
  - Ouvrez la fenêtre de configuration du préfixe WINE, via la commande donnée plus haut ;
  - Dans la fenêtre qui s’affiche, allez sur l’onglet "Affichage" ;
  - Cochez la case "Émuler un bureau virtuel", et dans les champs "Taille du bureau" renseignez la résolution qu’utilise votre bureau ;
  - Fermez la fenêtre avec "OK".
C’est fait, Divine Divinity se lancera maintenant dans un fenêtre adaptée à la résolution qu’il utilise. Vous pouvez à tout moment revenir à un mode plein écran en retournant dans la fenêtre de configuration de WINE et en décochant la case "Émuler un bureau virtuel".

==== Liens ====

  * [[https://en.wikipedia.org/wiki/Divine_Divinity|Divine Divinity sur Wikipedia]] (en anglais)
  * [[https://appdb.winehq.org/objectManager.php?sClass=version&iId=18392|entrée dans la base de donnée WineHQ AppDB]] (en anglais)
