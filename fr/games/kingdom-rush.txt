====== Kingdom Rush ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-kingdom-rush.sh|play-kingdom-rush.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/kingdom_rush|gog_kingdom_rush_2.0.0.5.sh]] (MD5 : a505372a8b3b0c98e0968301679e6781)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/kingdom-rush/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/kingdom-rush/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_kingdom_rush_2.0.0.5.sh
libplayit2.sh
play-kingdom-rush.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-kingdom-rush.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
