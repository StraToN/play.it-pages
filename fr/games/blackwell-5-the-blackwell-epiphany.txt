====== Blackwell 5: The Blackwell Epiphany ======

===== Description =====

[[https://www.dotslashplay.it/images/games/the-blackwell-epiphany/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/the-blackwell-epiphany/thumbnail.jpg?nocache }}]]

**The Blackwell Epiphany** : L’âme d’un mort crie contre la force d’un blizzard féroce. Il demande de l’aide. Il réclame des réponses.\\
Puis il crie comme s’il était déchiré comme du papier mouchoir humide.\\
Ce n’était pas la première fois, et ce ne sera pas la dernière.\\
La police n'est pas en mesure de l’arrêter, donc le devoir incombe à ceux qui le peuvent. Quelle force pourrait être si puissante - et si malveillante - qu'elle détruirait le noyau même d’une vie pour obtenir ce qu'elle veut ?\\
Rosa Blackwell et Joey Mallone ont l’intention de le découvrir, même si cela signifie prendre des risques.

  * age : +12 ans
  * catégorie : action, ésoterisme, Point'n Click
  * url officielle : [[http://www.wadjeteyegames.com/games/blackwell-ephiphany/|]]
  * date de sortie : 2014

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-the-blackwell-epiphany.sh|play-the-blackwell-epiphany.sh]] (mis à jour le 30/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/blackwell_epiphany_the|gog_blackwell_epiphany_2.0.0.2.sh]] (MD5 : 058091975ee359d7bc0f9d9848052296)
    * [[https://www.dotslashplay.it/ressources/the-blackwell-epiphany/|the-blackwell-epiphany_icons.tar.gz]] (MD5 : e0067ab5130b89148344c3dffaaab3e0)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_blackwell_epiphany_2.0.0.2.sh
libplayit2.sh
play-the-blackwell-epiphany.sh
the-blackwell-epiphany_icons.tar.gz
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-the-blackwell-epiphany.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
