====== Duet ======

===== Description =====

[[https://www.dotslashplay.it/images/games/duet/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/duet/thumbnail.jpg?nocache }}]]

**Duet** : Elégant et minimaliste, le Duet demande aux joueurs de simplement naviguer dans deux sphères en orbite à travers un dédale de plus en plus complexe d'obstacles qui tombent.\\
Au fur et à mesure que la difficulté augmente, la tension monte et un récit particulier se déploie. 

  * catégorie : arcade, action
  * url officielle : [[http://www.duetgame.com/|]]
  * date de sortie : 2013

===== Informations =====

//version vendue sur Humble Bundle//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-duet_humble-2016-03-03.sh|play-duet_humble-2016-03-03.sh]] (mis à jour le 11/04/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * Duet-Build1006023-Linux64.zip (MD5 : b9c34c29da94c199ee75a5e71272a1eb)
    * [[http://www.kumobius.com/presskits/duet/images/logo.png|logo.png]] (optionnel)
  * dépendances :
    * fakeroot
    * unzip

==== Utilisation ====

  - Installez les dépendances du script :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
Duet-Build1006023-Linux64.zip
logo.png
play-anything.sh
play-duet_humble-2016-03-03.sh
</code>
  - Lancez la construction du paquet depuis ce répertoire :<code>
$ sh ./play-duet_humble-2016-03-03.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

  * [[http://www.duetgame.com/|site officiel]] (en anglais)
  * [[https://en.wikipedia.org/wiki/Duet_(video_game)|article Wikipedia]] (en anglais)
