====== Spellforce ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-spellforce.sh|play-spellforce.sh]] (mis à jour le 17/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cibles :
    * [[https://www.gog.com/game/spellforce_platinum|setup_spellforce_-_platinum_edition_1.54.75000_(17748).exe]] (MD5 : ed34fb43d8042ff61a889865148b8dd2)
    * [[https://www.gog.com/game/spellforce_platinum|setup_spellforce_-_platinum_edition_1.54.75000_(17748)-1.bin]] (MD5 : 60fca014e7ccd4630a7ec3cedb23942a)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/spellforce/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/spellforce/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-spellforce.sh
setup_spellforce_-_platinum_edition_1.54.75000_(17748).exe
setup_spellforce_-_platinum_edition_1.54.75000_(17748)-1.bin
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-spellforce.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
