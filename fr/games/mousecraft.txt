====== MouseCraft ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-mousecraft.sh|play-mousecraft.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/mousecraft|gog_mousecraft_2.1.0.7.sh]] (MD5 : 5d99353d6ef0db693cc3a492cfb8ae4c)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/mousecraft/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/mousecraft/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_mousecraft_2.1.0.7.sh
libplayit2.sh
play-mousecraft.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-mousecraft.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Liens ====

  * [[http://mouse-craft.com/|site officiel]] (en anglais)
