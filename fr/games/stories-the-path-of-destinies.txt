====== Stories: The Path of Destinies ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-stories-the-path-of-destinies.sh|play-stories-the-path-of-destinies.sh]] (mis à jour le 9/02/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/stories_the_path_of_destinies|setup_stories_-_the_path_of_destinies_0.0.13825_(16929).exe]] (MD5 : 6f81dbadddbb4b30b4edda9ced9ddef8)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/stories-the-path-of-destinies/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/stories-the-path-of-destinies/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-stories-the-path-of-destinies.sh
setup_stories_-_the_path_of_destinies_0.0.13825_(16929).exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-stories-the-path-of-destinies.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
