====== War for the Overworld ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-war-for-the-overworld.sh|play-war-for-the-overworld.sh]] (mis à jour le 16/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cibles :
    * GOG :
      * [[https://www.gog.com/game/war_for_the_overworld|war_for_the_overworld_en_1_6_66_16455.sh]] (MD5 : 3317bba3d2ec7dc5715f0d44e6cb70c1)
      * optionnel : [[https://www.gog.com/game/war_for_the_overworld_underlord_edition_upgrade|gog_war_for_the_overworld_underlord_edition_upgrade_dlc_2.0.0.1.sh]] (MD5 : 635912eed200d45d8907ab1fb4cc53a4)
    * Humble Bundle :
      * War_for_the_Overworld_v1.5.2_-_Linux_x64.zip (MD5 : bedee8b966767cf42c55c6b883e3127c) - **[[#version_humble_obsolete_et_version_gog_gratuite|/!\ Cette version n’est plus mise à jour]]**
  * dépendances :
    * Arch Linux :
      * GOG :
        * libarchive
      * Humble Bundle :
        * unzip
    * Debian :
      * GOG :
        * bsdtar
        * fakeroot
      * Humble Bundle :
        * fakeroot
        * unzip

[[https://www.dotslashplay.it/images/games/war-for-the-overworld/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/war-for-the-overworld/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :
      - GOG :<code>
# pacman -S libarchive
</code>
      - Humble Bundle :<code>
# pacman -S unzip
</code>
    - Debian :
      - GOG :<code>
# apt-get install bsdtar fakeroot
</code>
      - Humble Bundle :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’archive :
    - GOG :<code>
$ ls
</code><code>
libplayit2.sh
play-war-for-the-overworld.sh
war_for_the_overworld_en_1_6_66_16455.sh
</code>
    - Humble Bundle :<code>
$ ls
</code><code>
libplayit2.sh
play-war-for-the-overworld.sh
War_for_the_Overworld_v1.5.2_-_Linux_x64.zip
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-war-for-the-overworld.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Version Humble obsolète et version GOG gratuite ====

<note>La version DRM-free de War for the Overworld anciennement vendue sur Humble Bundle ne sera plus mise à jour.</note>
Tous ceux qui possèdent cette version peuvent demander une version GOG gratuite, celle-ci est tenue à jour et est actuellement la seule version DRM-free officielle. Vous avez juste besoin d’envoyer un e-mail à support@brightrockgames.com incluant votre numéro de commande, et Brightrock Games prendra contact avec vous pour vous envoyer un code cadeau pour War for the Overworld qui vous pourrez enregistrer sur GOG.


Plus d’informations au sujet de la version DRM-free vendue sur GOG peuvent se trouver sur la page suivante (en anglais) :\\
[[https://brightrockgames.userecho.com/topics/2302-gog-drm-free-build-faq/|GOG & DRM-Free Build FAQ]]
