====== Strafe ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-strafe.sh|play-strafe.sh]] (mis à jour le 26/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cibles :
    * [[https://www.gog.com/game/strafe|setup_strafe_1.1_(16883).exe]] (MD5 : 4a867da5f23d688cb8f7695de9e24b05)
    * [[https://www.gog.com/game/strafe|setup_strafe_1.1_(16883)-1.bin]] (MD5 : 5d1dca6ef0035e262e14d3759cd09708)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/strafe/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/strafe/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-strafe.sh
setup_strafe_1.1_(16883).exe
setup_strafe_1.1_(16883)-1.bin
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-strafe.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
