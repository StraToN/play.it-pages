====== Little Big Adventure ======
//version vendue sur GOG//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-little-big-adventure.sh|play-little-big-adventure.sh]] (mis à jour le 11/09/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * setup_lba_2.1.0.22.exe (MD5 : c40177522adcbe50ea52590be57045f8)
  * dépendances :
    * fakeroot
    * innoextract
    * icoutils (optionnel)

[[https://www.dotslashplay.it/images/games/little-big-adventure/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/little-big-adventure/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.dosbox.com/|DOSBox]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot innoextract icoutils
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
setup_lba_2.1.0.22.exe
play-anything.sh
play-little-big-adventure.sh
</code>
  - Lancez la construction du paquet depuis ce répertoire :<code>
$ sh ./play-little-big-adventure.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

  * [[https://fr.wikipedia.org/wiki/Little_Big_Adventure|article Wikipédia]]
