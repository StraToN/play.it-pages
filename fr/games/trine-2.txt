====== Trine 2 Complete Story ======

=== version vendue sur GOG ===

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-trine-2_gog-2.0.0.4.sh|play-trine-2_gog-2.0.0.4.sh]]
  * cible :
    * gog_trine_2_complete_story_2.0.0.4.sh
  * dépendances :
    * fakeroot
    * unzip

=== version vendue sur Humble Bundle ===

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-trine-2_humblebundle-2014-10-16.sh|play-trine-2_humblebundle-2014-10-16.sh]]
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * trine2_complete_story_v2_01_build_425_humble_linux_full.zip
  * dépendances :
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/trine-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/trine-2/thumbnail.jpg?nocache }}]]

==== Utilisation (version vendue sur GOG) ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
gog_trine_2_complete_story_2.0.0.4.sh
play-anything.sh
play-trine-2_gog-2.0.0.4.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-trine-2_gog-2.0.0.4.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation (version vendue sur Humble Bundle) ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
play-anything.sh
play-trine-2_humblebundle-2014-10-16.sh
trine2_complete_story_v2_01_build_425_humble_linux_full.zip
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-trine-2_humblebundle-2014-10-16.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

[[https://en.wikipedia.org/wiki/Trine_2|Trine 2 sur Wikipedia]] (en anglais)
