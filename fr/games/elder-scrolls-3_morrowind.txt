====== The Elder Scrolls III: Morrowind ======

===== Description =====

[[https://www.dotslashplay.it/images/games/morrowind/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/morrowind/thumbnail.jpg?nocache }}]]

**The Elder Scrolls III: Morrowind**: Les prophéties perdues parlent d’un Incarné, la réincarnation d’un héros dunmer du nom de Nérévar, qui doit venir à Morrowind pour briser la malédiction qui l’accable. Afin d’accomplir la prophétie, l’empereur envoie un messager impérial inconnu sur l’île de Vvardenfell. Suite à diverses quêtes périlleuses et confrontations magiques, ce messager inconnu va devenir le héros le plus robuste de tout l’empire.

  * age : +18 ans
  * catégorie : RPG, action, aventure
  * url officielle : [[https://elderscrolls.bethesda.net/fr/morrowind|]]
  * date de sortie : 2002

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-morrowind.sh|play-morrowind.sh]] (mis à jour le 14/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cibles :
    * version anglaise :
      * [[https://www.gog.com/game/the_elder_scrolls_iii_morrowind_goty_edition|setup_tes_morrowind_goty_2.0.0.7.exe]] (MD5 : 3a027504a0e4599f8c6b5b5bcc87a5c6)
    * version française :
      * [[https://www.gog.com/game/the_elder_scrolls_iii_morrowind_goty_edition|setup_tes_morrowind_goty_french_2.0.0.7.exe]] (MD5 : 2aee024e622786b2cb5454ff074faf9b)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :
    - version anglaise :<code>
$ ls
</code><code>
libplayit2.sh
play-morrowind.sh
setup_tes_morrowind_goty_2.0.0.7.exe
</code>
    - version française :<code>
$ ls
</code><code>
libplayit2.sh
play-morrowind.sh
setup_tes_morrowind_goty_french_2.0.0.7.exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-morrowind.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
