====== Monkey Island 3: The Curse of Monkey Island ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-monkey-island-3.sh|play-monkey-island-3.sh]] (mis à jour le 1/04/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/the_curse_of_monkey_island|setup_the_curse_of_monkey_island_1.0_(18253).exe]] (MD5 : 20c74e5f60bd724182ec2bdbae6d9a49)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/monkey-island-3/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/monkey-island-3/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-monkey-island-3.sh
setup_the_curse_of_monkey_island_1.0_(18253).exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-monkey-island-3.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
