====== TRI: Of Friendship and Madness ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-tri-of-friendship-and-madness.sh|play-tri-of-friendship-and-madness.sh]] (mis à jour le 3/07/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/tri|gog_tri_2.6.0.9.sh]] (MD5 : c2bf151b58766740e52db9559d61e3d6)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

[[https://www.dotslashplay.it/images/games/tri-of-friendship-and-madness/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/tri-of-friendship-and-madness/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_tri_2.6.0.9.sh
libplayit2.sh
play-tri-of-friendship-and-madness.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-tri-of-friendship-and-madness.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
