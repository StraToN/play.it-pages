====== Beatbuddy: Tale of the Guardians ======

===== Description =====

[[https://www.dotslashplay.it/images/games/beatbuddy/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/beatbuddy/thumbnail.jpg?nocache }}]]

**Beatbuddy: Tale of the Guardians** est un jeu d'action musical 2D qui raconte l'aventure funky de l'être éthéré Beatbuddy.\\ 
Sa Symphonie natale musicale est en difficulté et il a besoin de votre aide contre les armées du prince gourmand Maestro !

  * age : tous
  * catégorie : action, musique
  * url officielle : [[http://www.beatbuddy.com/|]]
  * date de sortie : 2013

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-beatbuddy.sh|play-beatbuddy.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.humblebundle.com/store/beatbuddy|BeatbuddyLinux1439603370.zip]] (MD5 : 156d19b327a02ac4a277f6f6ad4e188e)
  * dépendances :
    * Arch Linux :
      * unzip
    * Debian :
      * fakeroot
      * unzip

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S unzip
</code>
    - Debian :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
BeatbuddyLinux1439603370.zip
libplayit2.sh
play-beatbuddy.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-beatbuddy.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Liens ====

  * [[http://beatbuddy.com/|site officiel de Beatbuddy]] (en anglais)
