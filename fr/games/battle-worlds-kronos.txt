====== Battle Worlds: Kronos ======

===== Description =====

[[https://www.dotslashplay.it/images/games/battle-worlds-kronos/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/battle-worlds-kronos/thumbnail.jpg?nocache }}]]

**Battle Worlds: Kronos** : Expérimentez les deux côtés d'une bataille en faisant appel à de multiples styles de combat, depuis la domination d'une civilisation avec une guerre tactique épique jusqu' à la sauvegarde de votre planète des forces militaires supérieures grâce à des tactiques de guérilla intelligentes.

  * age : +12 ans
  * catégorie : stratégie, tour-à-tour
  * url officielle : [[https://kingart-games.com/games/1-battle-worlds-kronos/|]]
  * date de sortie : 2016

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-battle-worlds-kronos.sh|play-battle-worlds-kronos.sh]] (mis à jour le 24/08/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/battle_worlds_kronos|gog_battle_worlds_kronos_2.1.0.6.sh]] (MD5 : 4d7f9e05524ff6b85139df903dbcb74b)
  * dépendances :
    * Arch Linux :
      * libarchive
    * Debian :
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_battle_worlds_kronos_2.1.0.6.sh
libplayit2.sh
play-battle-worlds-kronos.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-battle-worlds-kronos.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Liens ====

  * [[http://www.battle-worlds.com/|site officiel]] (en anglais)
