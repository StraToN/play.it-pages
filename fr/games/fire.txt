====== Fire! ======

===== Description =====

[[https://www.dotslashplay.it/images/games/fire/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/fire/thumbnail.jpg?nocache }}]]

**Fire** est un jeu d’exploration-aventure rempli de puzzles intuitifs. Sans le moindre mot, le jeu raconte une histoire désopilante.\\
Sillonnez l'âge de pierre et relevez le plus grand défi de l'époque : Trouver du feu !

  * age : +3 ans
  * catégorie : aventure, puzzle
  * date de sortie : 2015

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-fire.sh|play-fire.sh]] (mis à jour le 15/01/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.humblebundle.com/store/fire|LIN_ESD_1.0.6756_REL.zip]] (MD5 : eb990e8465afbca210009b1fca76adcb)
  * dépendances :
    * Arch Linux :
      * unzip
    * Debian :
      * fakeroot
      * unzip

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S unzip
</code>
    - Debian :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
LIN_ESD_1.0.6756_REL.zip
libplayit2.sh
play-fire.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-fire.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
