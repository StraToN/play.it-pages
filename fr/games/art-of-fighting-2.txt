====== Art of Fighting 2 ======

===== Description =====

[[https://www.dotslashplay.it/images/games/art-of-fighting-2/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/art-of-fighting-2/thumbnail.jpg?nocache }}]]

**Art of Fighting 2** : jeu vidéo de combat en 2D

  * catégorie : combat
  * date de sortie : 2008

===== Informations =====

//version vendue sur Humble Bundle//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-art-of-fighting-2_humble.sh|play-art-of-fighting-2_humble.sh]] (mis à jour le 9/04/2016)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
  * cible :
    * ArtOfFighting2.sh (MD5 : 835e36b56238c637ab39cac5e23e7b25)
  * dépendances :
    * fakeroot
    * unzip

==== Utilisation ====

  - Installez les dépendances des scripts :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire les scripts et l’installeur :<code>
$ ls
</code><code>
ArtOfFighting2.sh
play-anything.sh
play-art-of-fighting-2_humble.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-art-of-fighting-2_humble.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>


==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

  * [[https://en.wikipedia.org/wiki/Art_of_Fighting|Art of Fighting sur Wikipedia]] (en anglais)
