====== Edna & Harvey: Harvey’s New Eyes ======

===== Description =====

[[https://www.dotslashplay.it/images/games/edna-and-harvey-harveys-new-eyes/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/edna-and-harvey-harveys-new-eyes/thumbnail.jpg?nocache }}]]

**Edna & Harvey: Harvey’s New Eyes** : Lilli est la petite fille la plus sage du monde.\\
Elle s’acquitte de toutes ses tâches de façon diligente, aussi injustes soient-elles.\\
Sa manière innocente et imperturbable d’exécuter les ordres des autres a quelque chose de tragique et d’inquiétant.\\
Toute pensée colérique, toute désobéissance enfantine est enfouie profondément sous une surface apparemment impénétrable de douceur.\\
Mais combien de temps ces sentiments resteront-ils cachés ? Et d’où viennent les gentils petits gnomes et cette peinture avec une couleur rose agréable ? Sont-ils réels ou juste un produit de son subconscient ?

  * age : +12 ans
  * catégorie : aventure, Point'n Click
  * date de sortie : 2012

===== Informations =====

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-edna-and-harvey-harveys-new-eyes.sh|play-edna-and-harvey-harveys-new-eyes.sh]] (mis à jour le 28/12/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.gog.com/game/edna_harvey_harveys_new_eyes|gog_edna_harvey_harvey_s_new_eyes_2.0.0.1.sh]] (MD5 : fa6f7fd271fe63bbe71e3190e0596546)
  * dépendances :
    * Arch Linux:
      * libarchive
    * Debian:
      * bsdtar
      * fakeroot

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
# pacman -S libarchive
</code>
    - Debian :<code>
# apt-get install bsdtar fakeroot
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
gog_edna_harvey_harvey_s_new_eyes_2.0.0.1.sh
libplayit2.sh
play-edna-harvey-harveys-new-eyes.sh
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-edna-harvey-harveys-new-eyes.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>

==== Liens ====

  * [[https://en.wikipedia.org/wiki/Edna_%26_Harvey:_Harvey%27s_New_Eyes|article Wikipédia]] (en anglais)
