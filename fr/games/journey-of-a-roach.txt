====== Journey of a Roach ======

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-2/games/play-journey-of-a-roach.sh|play-journey-of-a-roach.sh]] (mis à jour le 24/06/2017)
    * [[https://www.dotslashplay.it/scripts/play.it-2/lib/libplayit2.sh|libplayit2.sh]] (mis à jour le 2/04/2018)
  * cible :
    * [[https://www.humblebundle.com/store/journey-of-a-roach|JoaR_1.3_PC_Full_Multi_Daedalic_ESD.exe]] (MD5 : e349a84f7b8ac095b06edea521cade8f)
  * dépendances :
    * Arch Linux :
      * icoutils
      * innoextract
    * Debian :
      * fakeroot
      * icoutils
      * innoextract

[[https://www.dotslashplay.it/images/games/journey-of-a-roach/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/journey-of-a-roach/thumbnail.jpg?nocache }}]]

<note>Le jeu installé via ce script utilisera [[https://www.winehq.org/|WINE]].</note>

==== Utilisation ====

  - Installez les dépendances des scripts :
    - Arch Linux :<code>
$ yaourt icoutils innoextract
</code>
    - Debian :<code>
# apt-get install fakeroot icoutils innoextract
</code>
  - Placez dans un même répertoire les scripts et l’archive :<code>
$ ls
</code><code>
libplayit2.sh
play-journey-of-a-roach.sh
JoaR_1.3_PC_Full_Multi_Daedalic_ESD.exe
</code>
  - Lancez la construction depuis ce répertoire :<code>
$ sh ./play-journey-of-a-roach.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu. Celle-ci devrait être similaire à :
    - Arch Linux :<code>
# pacman -U /chemin/vers/paquet_du_jeu.pkg.tar
</code>
    - Debian :<code>
# dpkg -i /chemin/vers/paquet_du_jeu.deb
# apt-get install -f
</code>
