====== Space Pirates And Zombies ======
//version vendue sur GOG//

  * scripts :
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-anything.sh|play-anything.sh]] (mis à jour le 18/03/2018)
    * [[https://www.dotslashplay.it/scripts/play.it-1/play-space-pirates-and-zombies.sh|play-space-pirates-and-zombies.sh]] (mis à jour le 16/05/2017)
  * cible :
    * gog_space_pirates_and_zombies_2.0.0.4.sh (MD5 : 46da2a84e78f8016e35f7c0e63e28581)
  * dépendances :
    * fakeroot
    * unzip

[[https://www.dotslashplay.it/images/games/space-pirates-and-zombies/screenshot.jpg|{{ https://www.dotslashplay.it/images/games/space-pirates-and-zombies/thumbnail.jpg?nocache }}]]

==== Utilisation ====

  - Installez les dépendances du script :<code>
# apt-get install fakeroot unzip
</code>
  - Placez dans un même répertoire scripts et installeur :<code>
$ ls
</code><code>
play-anything.sh
play-space-pirates-and-zombies.sh
gog_space_pirates_and_zombies_2.0.0.4.sh
</code>
  - Lancez la construction du paquet :<code>
$ sh ./play-space-pirates-and-zombies.sh
</code>
  - Patientez quelques minutes, la construction s’achèvera en vous donnant la série de commandes à lancer par root pour installer le jeu.

==== Utilisation avancée ====

Le script présenté ici peut prendre plusieurs options pour contrôler son comportement de manière plus fine. Suivez les liens pour avoir des détails sur leur utilisation.

  * [[:fr:commun:options-avancees#checksum|checksum]]
  * [[:fr:commun:options-avancees#compression|compression]]
  * [[:fr:commun:options-avancees#help|help]]
  * [[:fr:commun:options-avancees#prefix|prefix]]

==== Liens ====

[[https://en.wikipedia.org/wiki/Space_Pirates_and_Zombies|SPAZ sur Wikipedia (en anglais)]]
