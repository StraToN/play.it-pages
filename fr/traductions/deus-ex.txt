====== Deus Ex ======

[[https://www.dotslashplay.it/traductions/deus-ex/|index des archives de traduction]]

Vous trouverez sur la page donnée en lien une archive de traduction :
  * deusexfr.7z (~20 Mio)

Une fois l’archive téléchargée, il vous suffit de la décompresser à la racine du répertoire d’installation du jeu en remplaçant les fichiers déjà en place pour passer votre jeu en français.
